import math
import random

from livewires import games, color

# Let Livewires (Pygame under the hood) setup.
games.init(
        screen_width=800,
        screen_height=600,
        fps=60)


# A little hack to make collision detection work (when using Livewires).
def screen_tick():
    for obj in games.screen._objects[:]:
        if hasattr(obj, 'handle_collisions'):
            obj.handle_collisions()

    for timer in Timer.timers:
        timer.update()
games.screen.tick = screen_tick


# This isn't ideal -- yet.
class ParticleEffect(games.Animation):
    USE_COLLIDEABLES = False
    COLLIDEABLES = []
    NON_COLLIDEABLES = []

    COLISION_ATTACK_DAMAGE = 0

    def __init__(self, *args, is_collideable=True, **kwargs):
        self.mass = self.get_mass()
        super().__init__(*args, is_collideable=is_collideable, **kwargs)

    def get_mass(self):
        return 0

    def attack_damage(self):
        return self.COLISION_ATTACK_DAMAGE

    def is_collideable_object(self, obj):
        if (    not issubclass(obj.__class__, ParticleEffect) and
                (   self.USE_COLLIDEABLES and
                    obj.__class__ in self.COLLIDEABLES or
                    not self.USE_COLLIDEABLES and
                    obj.__class__ not in self.NON_COLLIDEABLES and
                    self.__class__ not in obj.NON_COLLIDEABLES and
                    not obj.USE_COLLIDEABLES)):
            return True
        return False

    def handle_collisions(self):
        for sprite in self.overlapping_sprites:
            if self.is_collideable_object(sprite):
                self.collide_with(sprite)

    def collide_with(self, obj): pass

    def die(self): self.destroy()


class Wrapper(games.Sprite):
    ''' An object that wraps around the screen. '''

    BASE_MASS = 1

    def __init__(self, *args, d_angle=0, is_collideable=False, **kwargs):
        self.d_angle = d_angle
        self.mass = self.get_mass() # An easier way to use mass equations?
        super().__init__(*args, is_collideable=is_collideable, **kwargs)

    def get_mass(self):
        ''' Override to use a custom equation. '''
        return self.BASE_MASS

    def get_radius(self):
        return (self.get_width() ** 2 + self.get_height() ** 2) ** .5

    def update(self):
        ''' Wrap objects when the they aren't visible (nothing fancy). '''

        if self.top > games.screen.height:
            self.bottom = 0
        elif self.bottom < 0:
            self.top = games.screen.height

        if self.left > games.screen.width:
            self.right = 0
        elif self.right < 0:
            self.left = games.screen.width

        self.angle += self.d_angle
        super().update()

    def die(self):
        self.destroy()


class Explosion(ParticleEffect):
    ''' Basic functionality of blast damage. '''

    SOUND = games.load_sound('explosion.wav')
    IMAGES = NotImplemented

    def __init__(self, x, y, damage=0):
        super().__init__(
                images=self.IMAGES,
                x=x,
                y=y,
                repeat_interval=4,
                n_repeats=1,
                # This makes the game run faster.
                is_collideable=False if damage == 0 else True)
        self.COLISION_ATTACK_DAMAGE = damage
        self.SOUND.play()


# You only need the images for Explosion subclasses.

class Spark(Explosion):
    IMAGES = [
            'spark1.bmp',
            'spark2.bmp',
            'spark3.bmp']

class Blast(Explosion):
    IMAGES = [
            'explosion1.bmp',
            'explosion2.bmp',
            'explosion3.bmp',
            'explosion4.bmp',
            'explosion5.bmp',
            'explosion6.bmp',
            'explosion7.bmp',
            'explosion8.bmp',
            'explosion9.bmp']

class SuperBlast(Explosion):
    IMAGES = [
            'blast1.bmp',
            'blast2.bmp',
            'blast3.bmp',
            'blast4.bmp',
            'blast5.bmp',
            'blast6.bmp',
            'blast7.bmp',
            'blast8.bmp',
            'blast9.bmp',
            'blast10.bmp']


class Collider(Wrapper):
    ''' An object that leaves an explosion behind when it dies. '''

    BASE_HEALTH = 1
    COLISION_ATTACK_DAMAGE = 0

    DAMPEN_FACTOR = 10000
    DIE_TYPE = Blast
    DIE_DAMAGE = 0

    USE_COLLIDEABLES = False
    COLLIDEABLES = []
    NON_COLLIDEABLES = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, is_collideable=True, **kwargs)

        self.health = self.BASE_HEALTH
        self.damagable = True
        self.died = False
        self.on_fire = False

    def attack_damage(self):
        return self.COLISION_ATTACK_DAMAGE

    def is_collideable_object(self, obj):
        if (    self.USE_COLLIDEABLES and
                obj.__class__ in self.COLLIDEABLES or
                not self.USE_COLLIDEABLES and
                obj.__class__ not in self.NON_COLLIDEABLES and
                self.__class__ not in obj.NON_COLLIDEABLES and
                not obj.USE_COLLIDEABLES):
            return True
        return False

    def handle_collisions(self):
        for sprite in self.overlapping_sprites:
            if self.is_collideable_object(sprite):
                self.collide_with(sprite)

    def collide_with(self, obj):
        if self.damagable:
            self.health -= obj.attack_damage()
            if self.health <= 0:
                self.health = 0
                self.die()

        # Repel self from the other object if it has mass.
        self.dx += (obj.dx - self.dx) * obj.mass / self.DAMPEN_FACTOR
        self.dy += (obj.dy - self.dy) * obj.mass / self.DAMPEN_FACTOR

    def die(self):
        self.died = True
        new_explosion = self.DIE_TYPE(
                x=self.x,
                y=self.y,
                damage=self.DIE_DAMAGE)
        games.screen.add(new_explosion)
        super().die()


class Asteroid(Collider):
    ''' Floating space rock. '''

    SMALL = 1
    MEDIUM = 2
    LARGE = 3
    IMAGES = {
            SMALL  : games.load_image('asteroid_small.bmp'),
            MEDIUM : games.load_image('asteroid_med.bmp'),
            LARGE  : games.load_image('asteroid_big.bmp')}

    BASE_SPEED = 2
    BASE_ROTATION_STEP = 6

    SPAWN = 2
    BASE_POINTS = 60
    BASE_HEALTH = 20
    COLISION_ATTACK_DAMAGE = 20
    BASE_MASS = 200

    breaks_left = 0

    def __init__(self, game, x, y, size, initial=False):
        if initial: # When a level starts, add up breaks to completion.
            Asteroid.breaks_left += self.get_breaks(size)
        self.game = game
        self.size = size

        super().__init__(
            image=self.IMAGES[size],
            x=x,
            y=y,
            dx=random.uniform(-1, 1) * self.BASE_SPEED / size,
            dy= random.uniform(-1, 1) * self.BASE_SPEED / size,
            d_angle= random.uniform(-1, 1) * self.BASE_ROTATION_STEP / size)

    def get_breaks(self, size):
        ''' Calculate the amount of asteroids that need to be broken. '''
        return 2 ** size - 1

    def get_mass(self):
        return self.size * self.BASE_MASS

    def die(self):
        ''' Unleash points, power ups, and asteroids. '''

        if not self.died:
            Asteroid.breaks_left -= 1
            try_adding_a_power_up(self.x, self.y)

            self.game.increase_score(self.BASE_POINTS // self.size)

            # If the asteroid isn't small, replace with smaller asteroids.
            if self.size != Asteroid.SMALL:
                for i in range(Asteroid.SPAWN):
                    new_asteroid = Asteroid(
                            game=self.game,
                            x=self.x,
                            y=self.y,
                            size=self.size - 1)
                    games.screen.add(new_asteroid)

            # If all asteroids are gone, advance to next level.
            if Asteroid.breaks_left == 0:
                self.game.advance()

            super().die()


class Directable:
    def _thrust(self, angle):
        angle = math.radians(angle) # In math we use radians.

        # Change velocity components based on angle.
        self.dx += self.VELOCITY_FORCE * math.sin(angle)
        self.dy += self.VELOCITY_FORCE * -math.cos(angle)

        # Cap velocity in each direction.
        self.dx = min(max(self.dx, -self.VELOCITY_MAX), self.VELOCITY_MAX)
        self.dy = min(max(self.dy, -self.VELOCITY_MAX), self.VELOCITY_MAX)

    def forward_thrust(self): self._thrust(self.angle)
    def reverse_thrust(self): self._thrust(self.angle + 180)

    def _rotate_thrust(self, amount):
        self.d_angle += amount
        self.d_angle = min(
                max(self.d_angle, -self.ROTATION_MAX), self.ROTATION_MAX)

    def clockwise_thrust(self): self._rotate_thrust(self.ROTATION_FORCE)
    def counter_clockwise_thrust(self):
        self._rotate_thrust(-self.ROTATION_FORCE)

    def apply_breaks(self):
        self.dx *= .97
        self.dy *= .97

        if -.02 < self.dx < .02: self.dx = 0
        if -.02 < self.dy < .02: self.dy = 0

    def apply_turning_breaks(self):
        self.d_angle *= .96
        if -.02 < self.d_angle < .02: self.d_angle = 0


class Timer:
    timers = []

    def __init__(self, obj, ticks, done=[], before=[], during=[], after=[]):
        self.timers.append(self)

        self.obj = obj
        self.ticks = ticks
        self.is_going = False

        self.done = done
        self.before = before
        self.during = during
        self.after = after

    def start(self, ticks=0):
        if self.is_going:
            self.call_functions(self.done)
        self.call_functions(self.before)
        self.timer = ticks if ticks > 0 else self.ticks
        self.is_going = True

    def update(self):
        if self.obj._gone:
            self.timers.remove(self)
        elif self.is_going:
            self.call_functions(self.during)
            self.timer -= 1
            if self.timer == 0:
                self.is_going = False
                self.call_functions(self.done)
        else:
            self.call_functions(self.after)

    def call_functions(self, functions):
        for func in functions: func()


class Weapons:
    LASER_DELAY = 12
    FIRE_DELAY = 30
    BOMB_DELAY = 1000
    BOMB_LOAD_DELAY = 4
    GUNS_SPEC = {'amount': 1, 'spacing': 0, 'rear': False}

    def shoot(self, weapon, angle=None):
        self._shoot(weapon, angle, **self.GUNS_SPEC)

    def _shoot(self, weapon, angle=None, amount=1, spacing=0, rear=False):
        angle = angle if angle is not None else self.angle
        right_angle = angle + 90

        offset = 0
        if amount % 2 == 0: offset = spacing / 2

        for shotpos in range(amount):
            shotpos -= amount // 2
            dx = (shotpos * spacing - offset) * math.sin(right_angle)
            dy = (shotpos * spacing - offset) * -math.cos(right_angle)

            new_weapon = weapon(
                    shot_by=self,
                    x=self.x + dx,
                    y=self.y + dy,
                    angle=angle,
                    radius=self.get_radius())
            games.screen.add(new_weapon)

            if rear:
                new_weapon = weapon(
                        shot_by=self,
                        x=self.x + dx,
                        y=self.y + dy,
                        angle=angle + 180,
                        radius=self.get_radius())
                games.screen.add(new_weapon)


def execute_if_pressed(keys, functions):
    for key in keys:
        if games.keyboard.is_pressed(key):
            for func in functions:
                func()


def try_adding_a_power_up(x, y, frequency=1):
    ''' Occasionally add a random power up when called. '''

    power_up_chances = [
            (Health, .05), (Shield, .01), (FastGuns, .005),
            (TripleGuns, .01), (RearGuns, .01)]

    r = random.random()
    chance_total = 0
    for power_up, chance in power_up_chances:
        chance_total += chance * frequency
        if r < chance_total:
            games.screen.add(power_up(x, y))
            return


class PowerUpable:
    SHIELD_IMAGE = games.load_image('shield_ship.bmp')

    HEALTH_TIME = 1
    SHIELD_TIME = 1000
    FAST_GUNS_TIME = 1000
    TRIPLE_GUNS_TIME = 1000
    REAR_GUNS_TIME = 1000

    HEALTH_AMOUNT = 400
    TRI_SPACING = 10

    LASER_SPEED_UP = 10
    BOMB_SPEED_UP = 800

    def initialize_power_ups(self):
        self.GUNS_SPEC = self.GUNS_SPEC.copy()

        self.power_ups = {
                Health: Timer(
                    self, self.HEALTH_TIME, before=[self.increase_health]),
                Shield: Timer(
                    self, self.SHIELD_TIME,
                    before=[self.shield], done=[self.no_shield]),
                FastGuns: Timer(
                    self, self.FAST_GUNS_TIME,
                    before=[self.fast_guns], done=[self.no_fast_guns]),
                TripleGuns: Timer(
                    self, self.TRIPLE_GUNS_TIME,
                    before=[self.triple_guns], done=[self.no_triple_guns]),
                RearGuns: Timer(
                    self, self.REAR_GUNS_TIME,
                    before=[self.rear_guns], done=[self.no_rear_guns])}

    def increase_health(self): self.health += self.HEALTH_AMOUNT

    def shield(self):
        self.set_image(self.SHIELD_IMAGE)
        self.damagable = False
    def no_shield(self):
        self.set_image(self.IMAGE)
        self.damagable = True

    def fast_guns(self):
        self.laser_timer.ticks -= self.LASER_SPEED_UP
        self.bomb_timer.ticks -= self.BOMB_SPEED_UP
    def no_fast_guns(self):
        self.laser_timer.ticks += self.LASER_SPEED_UP
        self.bomb_timer.ticks += self.BOMB_SPEED_UP

    def triple_guns(self):
        self.GUNS_SPEC['amount'] = 3
        self.GUNS_SPEC['spacing'] = self.TRI_SPACING
    def no_triple_guns(self):
        self.GUNS_SPEC['amount'] = 1
        self.GUNS_SPEC['spacing'] = 0

    def    rear_guns(self): self.GUNS_SPEC['rear'] = True
    def no_rear_guns(self): self.GUNS_SPEC['rear'] = False


# Is a class hierarchy the best way to do this???
class PowerUp(Collider):
    ''' Basic functionality for ship enhancements. '''

    IMAGE = NotImplemented
    LIFETIME = 1000
    DIE_TYPE = Spark
    BASE_SPEED = 1

    def __init__(self, x, y):
        self.life_timer = Timer(self, self.LIFETIME, done=[self.die])
        self.life_timer.start()
        super().__init__(
                image=self.IMAGE,
                x=x,
                y=y,
                dx=random.uniform(-1, 1) * self.BASE_SPEED,
                dy=random.uniform(-1, 1) * self.BASE_SPEED)

    def collide_with(self, obj):
        obj.power_ups[self.__class__].start()
        super().collide_with(obj)

class Health(PowerUp): IMAGE = games.load_image('health_power_up.bmp')
class Shield(PowerUp): IMAGE = games.load_image('shield_power_up.bmp')
class FastGuns(PowerUp): IMAGE = games.load_image('speed_shooter_power_up.bmp')
class TripleGuns(PowerUp): IMAGE = games.load_image('tri_shooter_power_up.bmp')
class RearGuns(PowerUp): IMAGE = games.load_image('rear_gun_power_up.bmp')


class Ship(Collider, Directable, Weapons, PowerUpable):
    ''' A player controlled object. '''

    STAR_TREK_IMAGE = games.load_image('ship1.bmp')
    STAR_WARS_IMAGE = games.load_image('ship2.bmp')
    RETRO_IMAGE = games.load_image('ship3.bmp')
    DEFAULT_IMAGE = STAR_TREK_IMAGE
    IMAGE = DEFAULT_IMAGE

    move_sound = games.load_sound('thrust.wav')
    ROTATION_FORCE = .1
    ROTATION_MAX = 3
    VELOCITY_FORCE = .03
    VELOCITY_MAX = 3

    COLISION_ATTACK_DAMAGE = 1
    BASE_HEALTH = 500
    BASE_MASS = 400

    def __init__(self, game, x, y):
        super().__init__(image=self.IMAGE, x=x, y=y)
        self.game = game

        self.initialize_power_ups()

        self.laser_timer = Timer(self, self.LASER_DELAY,
                after=[lambda :execute_if_pressed(
                    [games.K_SPACE],
                    [   lambda :self.shoot(Laser),
                        lambda :self.laser_timer.start()])])
        self.fire_timer = Timer(self, self.FIRE_DELAY,
                after=[lambda :execute_if_pressed(
                    [games.K_f],
                    [   lambda :self.shoot(FireBall),
                        lambda :self.fire_timer.start()])])
        self.bomb_timer = Timer(self, self.BOMB_DELAY,
                after=[lambda :execute_if_pressed(
                    [games.K_a],
                    [   lambda :self.bomb_fire_timer.start(),
                        lambda :self.bomb_timer.start()])])
        self.bomb_fire_timer = Timer(
                self, self.BOMB_LOAD_DELAY,
                done=[
                    lambda :self.shoot(Bomb),
                    lambda :self.laser_timer.start(3 * self.LASER_DELAY)])

        self.keys_to_check = {
                games.K_LEFT: [self.counter_clockwise_thrust],
                games.K_RIGHT: [self.clockwise_thrust],
                games.K_UP: [self.forward_thrust, self.move_sound.play],
                games.K_DOWN: [self.reverse_thrust, self.move_sound.play],
                games.K_q: [self.die],
                games.K_d: [self.apply_breaks]}

        self.health_display = games.Text(
                value=self.health,
                size=30,
                color=color.green,
                top=5,
                left=10,
                is_collideable=False)
        games.screen.add(self.health_display)

    def tick(self):
        # Why is time keeping here???
        self.game.time += 1 / games.screen.fps

    def update(self):
        self.sync_health_display()

        for key in self.keys_to_check.keys():
            if games.keyboard.is_pressed(key):
                for func in self.keys_to_check[key]: func()

        self.apply_turning_breaks() # Slow down any rotation.
        super().update()

    def sync_health_display(self):
        self.health_display.value = self.health
        self.health_display.left = 10 # Align to the left.

        # Color the text based on the value.
        if self.health < 3 * self.BASE_HEALTH:
            self.health_display.color = color.red
        elif self.health < 6 * self.BASE_HEALTH:
            self.health_display.color = color.yellow
        else:
            self.health_display.color = color.green

    def die(self):
        self.sync_health_display()
        self.game.end()
        super().die()


class Alien(Collider, Directable, Weapons):
    ''' A dangerous enemy! '''

    IMAGE = games.load_image('alien_ship.bmp')
    VELOCITY_FORCE = .1
    VELOCITY_MAX = .6
    BASE_HEALTH = 80
    COLISION_ATTACK_DAMAGE = 8
    POINTS = 100

    def __init__(self, game, x, y):
        self.wander = random.choice((True, False))
        self.target = game.ship
        self.game = game

        delay = self.LASER_DELAY
        self.laser_timer = Timer(self, delay, done=[
            self.conditional_shoot, lambda :self.laser_timer.start()])
        self.laser_timer.start(delay / 3) # Let the player react.

        super().__init__(image=self.IMAGE, x=x, y=y)

    def conditional_shoot(self):
        if not self.target.died:
            self.shoot(Laser, atan2(
                self.target.x - self.x,
                self.target.y - self.y))

    def update(self):
        if self.wander:
            self.d_angle += random.uniform(-10, 10) / 100
        elif not self.target.died:
            self.angle = atan2(
                    self.target.x - self.x,
                    self.target.y - self.y)

        self.forward_thrust()
        super().update()

    def die(self):
        self.game.increase_score(self.POINTS)
        try_adding_a_power_up(self.x, self.y, frequency=11)
        super().die()


class Projectile(Collider):
    def __init__(self, shot_by, x, y, angle, radius):
        super().__init__(
                image=self.IMAGE,
                angle=angle)

        rangle = math.radians(angle)

        self.x += x + (self.get_radius() + radius) * math.sin(rangle)
        self.y += y + (self.get_radius() + radius) * -math.cos(rangle)

        self.dx = self.VELOCITY * math.sin(rangle)
        self.dy = self.VELOCITY * -math.cos(rangle)


class Laser(Projectile):
    IMAGE = games.load_image('laser.bmp')
    SOUND = games.load_sound('laser.wav')

    BUFFER = 52

    VELOCITY = 8
    LIFETIME = 44

    COLISION_ATTACK_DAMAGE = 10
    DIE_TYPE = Spark

    def __init__(self, shot_by, x, y, angle, radius):
        super().__init__(shot_by, x, y, angle, radius)

        self.life_timer = Timer(self, self.LIFETIME, done=[self.destroy])
        self.life_timer.start()

        Laser.SOUND.play()


class Bomb(Projectile):
    IMAGE = games.load_image('bomb.bmp')
    SOUND = games.load_sound('missile.wav')
    VELOCITY = 5
    MOVE_TIME = 66
    LIFETIME = 400
    BUFFER = 54
    DIE_TYPE = SuperBlast
    DIE_DAMAGE = 6

    def __init__(self, shot_by, x, y, angle, radius):
        super().__init__(shot_by, x, y, angle, radius)

        self.move_timer = Timer(self, self.MOVE_TIME, done=[self.stop])
        self.life_timer = Timer(self, self.LIFETIME, done=[self.die])
        self.move_timer.start()
        self.life_timer.start()

        self.SOUND.play()

    def stop(self):
        self.dx = 0
        self.dy = 0


class FireBall(ParticleEffect):
    IMAGES = [
            'fireball1.bmp',
            'fireball2.bmp',
            'fireball3.bmp',
            'fireball4.bmp',
            'fireball5.bmp',
            'fireball6.bmp',
            'fireball7.bmp',
            'fireball8.bmp',
            'fireball9.bmp',
            'fireball10.bmp',
            'fireball11.bmp',
            'fireball12.bmp']
    BUFFER = 140

    def __init__(self, shot_by, x, y, angle, radius):
        # Put a little distance between the ship and fireball.
        rangle = math.radians(angle)
        x += math.sin(rangle) * FireBall.BUFFER
        y += -math.cos(rangle) * FireBall.BUFFER

        angle -= 90 # Correct the angle of the animation pictures.

        self.objects_hit = []

        super().__init__(
                images=self.IMAGES,
                x=x,
                y=y,
                angle=angle,
                n_repeats=1,
                repeat_interval=3,
                is_collideable=True)

    def collide_with(self, obj):
        # We don't want hundreds of fires on one object.
        if not obj.on_fire and obj not in self.objects_hit:
            self.objects_hit.append(obj)
            games.screen.add(Fire(obj))
        super().collide_with(obj)


class Fire(ParticleEffect):
    IMAGES = [
            'fire1.bmp',
            'fire2.bmp',
            'fire3.bmp',
            'fire4.bmp']
    COLISION_ATTACK_DAMAGE = .05
    LIFETIME = 1000
    SPRED_CHANCE = .01

    def __init__(self, obj):
        # Use this if you only want one fire per object; or not if you want
        # one fire per fireball.
        obj.on_fire = True

        self.obj = obj
        self.life_timer = Timer(self, self.LIFETIME, [self.die])
        super().__init__(
                images=self.IMAGES,
                x=obj.x,
                y=obj.y,
                n_repeats=-1,
                repeat_interval=6,
                is_collideable=True)

    def update(self):
        if self.obj._gone: self.die()

        self.x = self.obj.x
        self.y = self.obj.y
        super().update()

    def collide_with(self, obj):
        if (    random.random() < self.SPRED_CHANCE and
                obj is not self.obj and
                not obj.on_fire):
            games.screen.add(Fire(obj))
        super().collide_with(obj)

    def die(self):
        self.obj.on_fire = False
        super().die()


# These are out of their class definitions because all the classes are not
# defined yet, and this makes it simpler (although they should probably be
# somewhere in the game setup).
PowerUp.USE_COLLIDEABLES = True
PowerUp.COLLIDEABLES = [Ship]
Asteroid.NON_COLLIDEABLES = [Asteroid, Alien]
Alien.NON_COLLIDEABLES = [Asteroid, Alien]
Laser.NON_COLLIDEABLES = [Laser]
Fire.NON_COLLIDEABLES = [Laser]
FireBall.NON_COLLIDEABLES = [Laser]


class Game(object):
    # Load sound for level advance
    SOUND = games.load_sound('level.wav')

    menu_color = color.blue
    best_stats_color = color.red
    latest_stats_color = color.yellow
    instructions_color = color.black

    ALIEN_MIN_TIME = 3000
    ALIEN_MAX_TIME = 6000

    def __init__(self):
        self.load_stats()

    def initialize(self):
        self.level = 0
        self.time = 0

        self.score = games.Text(
                value=0,
                size=30,
                color=color.white,
                top=5,
                right=games.screen.width - 10,
                is_collideable=False)
        games.screen.add(self.score)

        # The player's ship
        self.ship = Ship(
                game=self,
                x=games.screen.width / 2,
                y=games.screen.height / 2)
        games.screen.add(self.ship)

        self.alien_min_time = self.ALIEN_MIN_TIME
        self.alien_max_time = self.ALIEN_MAX_TIME

        self.alien_timer = Timer(self.ship, 0,
                done=[self.create_alien, self.set_time_until_next_alien,
                    lambda :self.alien_timer.start()])
        self.set_time_until_next_alien()
        self.alien_timer.start()

    def increase_alien_difficulty(self):
        if self.alien_min_time > 300:
            self.alien_min_time -= 200
        if self.level % 5 == 0 and self.alien_max_time > 1500:
            self.alien_max_time -= 1000

    def create_alien(self):
        alien = Alien(self,
                random.randint(0, games.screen.width),
                random.randint(0, games.screen.height))
        games.screen.add(alien)

    def set_time_until_next_alien(self):
        self.alien_timer.ticks = random.randint(
                self.alien_min_time, self.alien_max_time)

    def play(self):
        # Begin theme music
        games.music.load('theme.mid')
        games.music.play(-1)

        games.screen.background = games.load_image('nebula.jpg')
        self.show_menu()

        # Let the games begin!
        games.screen.mainloop()

    def advance(self):
        self.level += 1
        self.increase_alien_difficulty()

        # Amount of space around ship to preserve when creating asteroids.
        BUFFER = 200

        # Create new asteroids
        for i in range(self.level):
            # Choose a minimum distance away from the ship along the x and
            # y-axis that adds up to BUFFER.
            x_min = random.randrange(BUFFER)
            y_min = BUFFER - x_min

            # Choose a distance along x and y-axis based on the ship being
            # at the screen edges.
            x_distance = random.randrange(
                    x_min,
                    games.screen.width - x_min)
            y_distance = random.randrange(
                    y_min,
                    games.screen.height - y_min)

            # Calculate the actual location based on distance from the ship,
            # and wrapping around screen if necessary.
            x = (self.ship.x + x_distance) % games.screen.width
            y = (self.ship.y + y_distance) % games.screen.height

            new_asteroid = Asteroid(
                    game=self,
                    x=x,
                    y=y,
                    size=Asteroid.LARGE,
                    initial=True)
            games.screen.add(new_asteroid)

        # Briefly display the level number.
        level_message = games.Message(
                value='Level ' + str(self.level),
                size=40,
                color=color.yellow,
                x=games.screen.width / 2,
                y=games.screen.width / 10,
                lifetime=4 * games.screen.fps, # 4 seconds
                is_collideable=False)
        games.screen.add(level_message)

        # Play "next level" sound (except on first level).
        if self.level > 1:
            self.SOUND.play()

    def increase_score(self, amount):
        self.score.value += amount
        # Make sure the score is still aligned correctly.
        self.score.right = games.screen.width - 10

    def end(self):
        self.update_stats()

        # Show "Game Over" for 6 seconds before returning to the menu.
        end_message = games.Message(
                value='Game Over',
                size=90,
                color=color.red,
                x=games.screen.width / 2,
                y=games.screen.height / 2,
                lifetime=6 * games.screen.fps,
                after_death=self.show_menu,
                is_collideable=False)
        games.screen.add(end_message)

    def show_menu(self):
        self.reset()
        self.place_menus(
                x=games.screen.width / 2,
                y=0,
                menus=[
                    ['New Game (press ENTER)',
                        [(games.K_RETURN, self.restart)]],
                    ['Stats (press S)',
                        [(games.K_s, self.show_stats)]],
                    ['Options (press O)',
                        [(games.K_o, self.show_options)]],
                    ['Instructions (press I)',
                        [(games.K_i, self.show_instructions)]],
                    ['Quit (press Q)',
                        [(games.K_q, self.quit)]]])

    def place_menus(self, x, y, menus):
        menu_area_size = (games.screen.height - y) / len(menus)
        # Place menus in the center of the space given.
        y = y + .5 * menu_area_size
        for value, responses in menus:
            new_menu = games.Question( # Seems like a menu class would do.
                    value=value,
                    size=40,
                    color=self.menu_color,
                    x=x,
                    y=y,
                    is_collideable=False,
                    responses=responses)
            games.screen.add(new_menu)
            y += menu_area_size

    def show_stats(self):
        self.reset() # Clear screen
        y = 60
        self.show_return_menu(y)
        y *= 3

        x = .25 * games.screen.width
        self.place_text(x, y, self.latest_stats_color, (
            'Latest Stats',
            'Asteroids left to break: {}'.format(
                self.latest_breaks_left),
            'Score: {}'.format(self.latest_score),
            'Level: {}'.format(self.latest_level),
            'Time: {} seconds'.format(round(self.latest_time, 2))))

        x = .75 * games.screen.width
        self.place_text(x, y, self.best_stats_color, (
            'Best Stats',
            'Asteroids left to break: {}'.format(
                self.high_score_breaks_left),
            'Score: {}'.format(self.high_score),
            'Level: {}'.format(self.high_score_level),
            'Time: {} seconds'.format(round(self.high_score_time, 2))))

    def place_text(self, x, y, text_color, lines):
        line_height = 30
        for line in lines:
            new_text = games.Text(
                    value=line,
                    size=line_height,
                    color=text_color,
                    x=x,
                    y=y,
                    is_collideable=False)
            games.screen.add(new_text)
            y += line_height

    def load_stats(self):
        # Using line, instead of file, makes the code clearer.
        with open('stats') as line:
            self.latest_score = int(next(line))
            self.latest_level = int(next(line))
            self.latest_time = float(next(line))
            self.latest_breaks_left = int(next(line))

            self.high_score = int(next(line))
            self.high_score_level = int(next(line))
            self.high_score_time = float(next(line))
            self.high_score_breaks_left = int(next(line))

    def update_stats(self):
        self.latest_score = self.score.value
        self.latest_level = self.level
        self.latest_time = self.time
        self.latest_breaks_left = Asteroid.breaks_left

        if self.latest_score > self.high_score:
            self.high_score = self.latest_score
            self.high_score_level = self.latest_level
            self.high_score_time = self.latest_time
            self.high_score_breaks_left = self.latest_breaks_left
        elif self.latest_score == self.high_score:
            if self.latest_level > self.high_score_level:
                self.high_score_level = self.latest_level
            if self.latest_time < self.high_score_time:
                self.high_score_time = self.latest_time
            if self.latest_breaks_left < self.high_score_breaks_left:
                self.high_score_breaks_left = self.latest_breaks_left

        self.save_stats()

    def save_stats(self):
        with open('stats', 'w') as file:
            file.write('\n'.join((
                str(self.latest_score),
                str(self.latest_level),
                str(self.latest_time),
                str(self.latest_breaks_left),

                str(self.high_score),
                str(self.high_score_level),
                str(self.high_score_time),
                str(self.high_score_breaks_left))))

    def show_options(self):
        self.reset() # Clear screen
        y = 60
        self.show_return_menu(y)
        y *= 3
        x = games.screen.width / 2
        self.place_menus(
                x=x,
                y=y,
                menus=[
                    ['Star Trek Style Ship (press 1 or D)',
                        [
                            (games.K_d, lambda :self.change_ship_image(
                                Ship.DEFAULT_IMAGE)),
                            (games.K_1, lambda :self.change_ship_image(
                                Ship.STAR_TREK_IMAGE))]],
                    ['Star Wars Style Ship (press 2)',
                        [(games.K_2, lambda :self.change_ship_image(
                            Ship.STAR_WARS_IMAGE))]],
                    ['Retro Style Ship (press 3)',
                        [(games.K_3, lambda :self.change_ship_image(
                            Ship.RETRO_IMAGE))]]])

        self.show_current_ship()

    def change_ship_image(self, image):
        Ship.IMAGE = image
        self.show_current_ship()

    def show_current_ship(self):
        try:
            self.ship_sprite.destroy()
        except AttributeError: pass

        self.ship_sprite = games.Sprite(
                image=Ship.IMAGE, x=80, y=games.screen.height / 2)
        games.screen.add(self.ship_sprite)

    def show_instructions(self):
        self.reset()
        y = 60
        self.show_return_menu(y)
        y *= 3
        x = games.screen.width / 2
        self.place_text(x, y, self.instructions_color, (
            'SHIP CONTROLS',
            'up arrow -> forward thrust',
            'down arrow -> backward thrust',
            'left arrow -> rotate counter-clockwise',
            'right arrow -> rotate clockwise',
            'space bar -> laser gun',
            'A -> bomb',
            'F -> flame thrower',
            'D -> break/slow down',
            'Q -> end game and return to menu'))

    def show_return_menu(self, y):
        new_menu = games.Question(
                value='Return To Menu (press M)',
                size=40,
                color=self.menu_color,
                x=games.screen.width / 2,
                y=y,
                is_collideable=False,
                responses=[(games.K_m, self.show_menu)])
        games.screen.add(new_menu)

    def reset(self):
        Asteroid.breaks_left = 0 # This should be in the initialize.
        games.screen.clear()

    def restart(self):
        self.reset()
        self.initialize()
        self.advance()

    def quit(self):
        games.screen.quit()


def atan2(x, y):
    # Add pi/2 to account for 0 degrees being up not right.
    return math.degrees(math.atan2(y, x) + math.pi / 2)


def main():
    astrocrash = Game()
    astrocrash.play()

if __name__ == '__main__':
    main()
